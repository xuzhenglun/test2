package main

import (
	"log"
	"net"
	"strconv"
	"time"
)

func handle(i int) {
	conn, err := net.Dial("tcp", "127.0.0.1:1080")
	if err != nil {
		log.Println(err)
	}
	defer func() {
		if conn != nil {
			conn.Close()
		}
	}()
	if _, err = conn.Write([]byte(strconv.Itoa(i))); err != nil {
		log.Println(err)
	}
}

func main() {
	for {
		for i := 0; i < 10000; i++ {
			handle(i)
		}
		time.Sleep(1000)
	}
}
